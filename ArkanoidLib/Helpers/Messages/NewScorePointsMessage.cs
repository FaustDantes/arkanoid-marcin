﻿namespace ArkanoidLib.Helpers.Messages
{
   public class NewScorePointsMessage
   {
       public int NewPoints;

       public NewScorePointsMessage(int newPoints)
       {
           NewPoints = newPoints;
       }
   }
}
