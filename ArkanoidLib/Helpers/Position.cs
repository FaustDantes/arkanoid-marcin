﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ArkanoidLib.Helpers
{
    public class Position
    {
        public int Left;
        public int Right;
        public int Top;

        public Position(int left, int right, int top, int width, int height)
        {
            Left = left;
            Right = right;
            Top = top;

            topCorner.X = left;
            topCorner.Y = top;

            bottomCorner.X = left + width;
            bottomCorner.Y = top + height;
        }

        private Coordinates topCorner;
        private Coordinates bottomCorner;
    }
}
