﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using ArkanoidLib.Helpers;

namespace ArkanoidLib.Models
{
    public  class BrickModel
    {
        public readonly Guid BrickId;
        private Position position;
        public readonly int BrickPrice;

        public bool IsAvailable;

        public BrickModel( int brickPrice, Position position)
        {
            this.BrickPrice = brickPrice;
            this.position = position;
            IsAvailable = true;
            BrickId= Guid.NewGuid();
        }
        public Position GetBrickCoordinates()
        {
            return position;
        }
    }
}
