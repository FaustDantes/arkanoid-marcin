﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Arkanoid.ViewModels
{
    public class BrickViewModel
    {
        public Thickness BrickPosition { get; set; }
        public readonly Guid BrickId;
        public int Price { get; set; }
        public bool IsVisible;

        public BrickViewModel(Thickness brickPosition, int price, Guid brickId)
        {
            BrickPosition = brickPosition;
            BrickId = brickId;
            Price = price;
        }
    }
}
