﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Windows;

namespace Arkanoid.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        internal abstract void InitializeViewModel();
    }
}
