﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArkanoidLib;
using ArkanoidLib.Helpers;
using ArkanoidLib.Helpers.Messages;

namespace Arkanoid.ViewModels
{
    public class StatisticsViewModel : ViewModelBase
    {
        private IMessenger messenger;
        private int score;
        public int Score
        {
            get => score;
            set
            {
                this.score = value;
                OnPropertyChanged();
            }
        }

        private int life;
        public int Life
        {
            get => life;
            set
            {
                this.life = value;
                OnPropertyChanged();
            }
        }

        public StatisticsViewModel(IMessenger messenger)
        {
            InitializeViewModel();

            this.messenger = messenger;
            messenger.Register<NewScorePointsMessage>(NewScorePointsMessageReceivedFunction);
        }

        internal sealed override void InitializeViewModel()
        {
            score = DefaultValues.DEFAULT_SCORE;
            life = DefaultValues.DEFAULT_LIFE_COUNT;
        }

        private void NewScorePointsMessageReceivedFunction(NewScorePointsMessage message)
        {
            Score += message.NewPoints;
        }

    }
}
