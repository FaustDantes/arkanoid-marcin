﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Arkanoid.Converters;
using ArkanoidLib;
using ArkanoidLib.Helpers;
using ArkanoidLib.Helpers.Messages;
using ArkanoidLib.Models;

namespace Arkanoid.ViewModels
{
    public class GameFieldViewModel : ViewModelBase
    {
        private IMessenger messenger;

        public ICommand OnMouseMoveCallback { get; set; }

        private Thickness gameBarPosition;
        public Thickness GameBarPosition
        {
            get => gameBarPosition;
            set
            {
                gameBarPosition = value;
                OnPropertyChanged();
            }
        }


        private ObservableCollection<BrickViewModel> bricks = new ObservableCollection<BrickViewModel>();
        public ObservableCollection<BrickViewModel> Bricks
        {
            get => bricks;
            set
            {
                bricks = value;
                OnPropertyChanged();
            }
        }

        public GameFieldViewModel(IMessenger messenger)
        {
            this.messenger = messenger;

            InitializeViewModel();
        }


        internal sealed override void InitializeViewModel()
        {
            gameBarPosition = new Thickness(190,470,190,0);

            List<BrickViewModel> gameBricks = GetGameBricks(4,100);
            gameBricks.AddRange(GetGameBricks(4,50,40));

            Bricks = new ObservableCollection<BrickViewModel>(gameBricks);
        }

        private List<BrickViewModel> GetGameBricks(int bCount, int price, int ofset =0)
        {
            List<BrickViewModel> br = new List<BrickViewModel>();
            for (int i = 0; i < bCount; i++)
            {
                br.Add(new BrickViewModel(new Thickness(20+60*i, 80 + ofset, 0, 0), price,Guid.NewGuid()));
            }

            return br;
        }


        private static void MouseDownCommandPropertyChangedCallBack(DependencyObject inDependencyObject, DependencyPropertyChangedEventArgs inEventArgs)
        {
            UIElement uiElement = inDependencyObject as UIElement;
            if (null == uiElement)
                return;

            uiElement.MouseDown += (sender, args) =>
            {
              //  GetMouseDownCommand(uiElement).Execute(args);
                args.Handled = true;
            };
        }
    }
}
