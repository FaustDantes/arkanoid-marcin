﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arkanoid.ViewModels;
using ArkanoidLib;

namespace Arkanoid
{
    public class ViewModelLocator
    {
        private static readonly Messenger messenger = new Messenger();

        public MainViewModel MainViewModel => CreateMainViewModel();
        private MainViewModel CreateMainViewModel()
        {
            return new MainViewModel(messenger);
        }

        public StatisticsViewModel StatisticsViewModel => CreateStatisticsViewModel();
        private StatisticsViewModel CreateStatisticsViewModel()
        {
            return new StatisticsViewModel(messenger);
        }

        public GameFieldViewModel GameFieldViewModel => CreateGameFieldViewModel();
        private GameFieldViewModel CreateGameFieldViewModel()
        {
            return  new GameFieldViewModel(messenger);
        }
    }
}
