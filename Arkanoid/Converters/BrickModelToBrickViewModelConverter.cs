﻿using System.Windows;
using System.Windows.Media;
using Arkanoid.ViewModels;
using ArkanoidLib.Models;

namespace Arkanoid.Converters
{
    public static class BrickModelToBrickViewModelConverter
    {
        public static BrickViewModel Convert(BrickModel brick)
        {
            var coordinates = brick.GetBrickCoordinates();
            var position = new Thickness(coordinates.Left,coordinates.Top,coordinates.Right,0);
            return new  BrickViewModel(position,brick.BrickPrice, brick.BrickId);
        }

       // public static BrickModel ConvertBack(BrickViewModel brickViewModel)
       // {
       // }


    }
}
